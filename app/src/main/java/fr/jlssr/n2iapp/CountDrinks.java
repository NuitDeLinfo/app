package fr.jlssr.n2iapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class CountDrinks extends AppCompatActivity
{

    //Points max : 70

    //Nomre de points à attribuer
    private int points;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_drinks);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
        final TextView seekBarValue = (TextView)findViewById(R.id.seekBarValue);

        final ImageView imageMilieu = (ImageView)findViewById(R.id.binouzeMilieu);
        final ImageView imageMilieuGauche = (ImageView)findViewById(R.id.binouzeMilieuGauche);
        final ImageView imageMilieuDroite = (ImageView)findViewById(R.id.binouzeMilieuDroite);
        final ImageView imageGauche = (ImageView)findViewById(R.id.binouzeGauche);
        final ImageView imageDroite = (ImageView)findViewById(R.id.binouzeDroite);
        final ImageView imageMort = (ImageView)findViewById(R.id.mort);

        imageMilieu.setVisibility(ImageView.INVISIBLE);
        imageMilieuGauche.setVisibility(ImageView.INVISIBLE);
        imageMilieuDroite.setVisibility(ImageView.INVISIBLE);
        imageGauche.setVisibility(ImageView.INVISIBLE);
        imageDroite.setVisibility(ImageView.INVISIBLE);
        imageMort.setVisibility(ImageView.INVISIBLE);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                if(progress <= 1)
                {
                    seekBarValue.setText(String.valueOf("Tu es raisonnable, c'est bien !"));
                    imageMilieu.setVisibility(ImageView.INVISIBLE);
                    imageMilieuGauche.setVisibility(ImageView.INVISIBLE);
                    imageMilieuDroite.setVisibility(ImageView.INVISIBLE);
                    imageGauche.setVisibility(ImageView.INVISIBLE);
                    imageDroite.setVisibility(ImageView.INVISIBLE);
                    imageMort.setVisibility(ImageView.INVISIBLE);
                    points = 0;
                }
                else if(progress <= 3)
                {
                    seekBarValue.setText(String.valueOf("Il va falloir penser à s'arréter"));
                    imageMilieuGauche.setVisibility(ImageView.INVISIBLE);
                    imageMilieuDroite.setVisibility(ImageView.INVISIBLE);
                    imageGauche.setVisibility(ImageView.INVISIBLE);
                    imageDroite.setVisibility(ImageView.INVISIBLE);
                    imageMilieu.setVisibility(ImageView.VISIBLE);
                    imageMort.setVisibility(ImageView.INVISIBLE);
                    points = 2;
                }
                else if(progress <= 5)
                {
                    seekBarValue.setText(String.valueOf("Il faut s'arréter maintenant :)"));
                    imageMilieu.setVisibility(ImageView.VISIBLE);
                    imageGauche.setVisibility(ImageView.INVISIBLE);
                    imageDroite.setVisibility(ImageView.INVISIBLE);
                    imageMilieuGauche.setVisibility(ImageView.VISIBLE);
                    imageMilieuDroite.setVisibility(ImageView.VISIBLE);
                    imageMort.setVisibility(ImageView.INVISIBLE);
                    points = 5;
                }
                else if(progress <= 10)
                {
                    seekBarValue.setText(String.valueOf("Wow attention mon pote là"));
                    imageMilieu.setVisibility(ImageView.VISIBLE);
                    imageGauche.setVisibility(ImageView.INVISIBLE);
                    imageDroite.setVisibility(ImageView.INVISIBLE);
                    imageMilieuGauche.setVisibility(ImageView.VISIBLE);
                    imageMilieuDroite.setVisibility(ImageView.VISIBLE);
                    imageMort.setVisibility(ImageView.INVISIBLE);
                    points = 10;
                }
                else if(progress <= 15)
                {
                    seekBarValue.setText(String.valueOf("Tu tiens encore debout là ?"));
                    imageGauche.setVisibility(ImageView.VISIBLE);
                    imageDroite.setVisibility(ImageView.VISIBLE);
                    imageMilieu.setVisibility(ImageView.VISIBLE);
                    imageMilieuGauche.setVisibility(ImageView.VISIBLE);
                    imageMilieuDroite.setVisibility(ImageView.VISIBLE);
                    imageMort.setVisibility(ImageView.INVISIBLE);
                    points = 15;
                }
                else if(progress <= 20)
                {
                    String message = "Euh vraiment," + progress + " verres ?!";
                    seekBarValue.setText(String.valueOf(message));
                    imageGauche.setVisibility(ImageView.VISIBLE);
                    imageDroite.setVisibility(ImageView.VISIBLE);
                    imageMilieu.setVisibility(ImageView.VISIBLE);
                    imageMilieuGauche.setVisibility(ImageView.VISIBLE);
                    imageMilieuDroite.setVisibility(ImageView.VISIBLE);
                    imageMort.setVisibility(ImageView.VISIBLE);
                    points = 50 + progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });
    }

    public void valider(View view) {
        ScoreSingleton.getInstance().addPoints(this.points);
        Intent intent = new Intent(getApplicationContext(), TitleBrakeInTime.class);
        startActivity(intent);

    }
}
