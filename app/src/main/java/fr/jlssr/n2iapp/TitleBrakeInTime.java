package fr.jlssr.n2iapp;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TitleBrakeInTime extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_brake_in_time);

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {}

            public void onFinish() {
                Intent goQuestion = new Intent(getApplicationContext(), BrakeInTime.class);
                startActivity(goQuestion);
                finish();
            }
        }.start();
    }
}
