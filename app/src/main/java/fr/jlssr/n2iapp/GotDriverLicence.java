package fr.jlssr.n2iapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GotDriverLicence extends AppCompatActivity {

    private Button maybe;
    private Button yes;
    private Button no;

    //Points max : 50

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_got_driver_licence);

        this.maybe = findViewById(R.id.btn_maybe);
        this.maybe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScoreSingleton s = ScoreSingleton.getInstance();
                s.addPoints(5);
                Intent nextQuestion = new Intent(getApplicationContext(), TitleBrakeInTime.class);
                startActivity(nextQuestion);
                finish();
            }
        });

        this.yes = findViewById(R.id.btn_yes);
        this.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextQuestion = new Intent(getApplicationContext(), TitleBrakeInTime.class);
                startActivity(nextQuestion);
                finish();
            }
        });

        this.no = findViewById(R.id.btn_no);
        this.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScoreSingleton.getInstance().addPoints(50);
                startActivity(new Intent(getApplicationContext(), Score_final.class));
                finish();
            }
        });
    }
}
