package fr.jlssr.n2iapp;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BrakeInTime extends AppCompatActivity {

    private static boolean clicknow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brake_in_time);

        BrakeInTime.clicknow = false;

        new CountDownTimer(10000, 1000) { // 10 seconds

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                BrakeInTime.clicknow = true;
                ImageView charac = findViewById(R.id.charac);
                charac.setVisibility(View.VISIBLE);
            }
        }.start();

        new CountDownTimer(1000, 1000) { // 1 second

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                BrakeInTime.clicknow = false;
                ImageView charac = findViewById(R.id.charac);
                charac.setVisibility(View.INVISIBLE);
            }
        }.start();


    }

    public void imgclick(View view) {
        view.setVisibility(View.INVISIBLE);
        ImageView pedaleEnf = findViewById(R.id.pedale_enf);
        pedaleEnf.setVisibility(View.VISIBLE);

        if (BrakeInTime.clicknow) {
            TextView success = findViewById(R.id.success);
            success.setVisibility(View.VISIBLE);
            startActivity(new Intent(getApplicationContext(), Score_final.class));
            finish();
        } else {
            TextView failed = findViewById(R.id.failed);
            failed.setVisibility(View.VISIBLE);
            ScoreSingleton.getInstance().addPoints(20);
            startActivity(new Intent(getApplicationContext(), Score_final.class));
            finish();
        }

    }

}
