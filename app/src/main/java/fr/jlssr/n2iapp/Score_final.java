package fr.jlssr.n2iapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Score_final extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int points_tot = ScoreSingleton.getInstance().getPoints();
        String message = points_tot + " / 130";
        setContentView(R.layout.activity_score_final);
        TextView view_score = findViewById(R.id.score);
        view_score.setText(String.valueOf(message));

        final ImageView imageContent = findViewById(R.id.content);
        final ImageView imagePasContent = findViewById(R.id.pas_content);
        final ImageView imagePasBien = findViewById(R.id.pas_bien);
        final ImageView imageAh = findViewById(R.id.ah);
        final ImageView imageMort = findViewById(R.id.mort_smiley);

        if(points_tot < 10)
        {
            imageContent.setVisibility(ImageView.VISIBLE);
            imagePasContent.setVisibility(ImageView.INVISIBLE);
            imagePasBien.setVisibility(ImageView.INVISIBLE);
            imageAh.setVisibility(ImageView.INVISIBLE);
            imageMort.setVisibility(ImageView.INVISIBLE);
        }
        else if(points_tot < 30)
        {
            imagePasContent.setVisibility(ImageView.VISIBLE);
            imageContent.setVisibility(ImageView.INVISIBLE);
            imagePasBien.setVisibility(ImageView.INVISIBLE);
            imageAh.setVisibility(ImageView.INVISIBLE);
            imageMort.setVisibility(ImageView.INVISIBLE);
        }
        else if(points_tot < 60)
        {
            imageContent.setVisibility(ImageView.INVISIBLE);
            imagePasContent.setVisibility(ImageView.INVISIBLE);
            imagePasBien.setVisibility(ImageView.VISIBLE);
            imageAh.setVisibility(ImageView.INVISIBLE);
            imageMort.setVisibility(ImageView.INVISIBLE);
        }
        else if(points_tot < 90)
        {
            imageContent.setVisibility(ImageView.INVISIBLE);
            imagePasContent.setVisibility(ImageView.INVISIBLE);
            imagePasBien.setVisibility(ImageView.INVISIBLE);
            imageAh.setVisibility(ImageView.VISIBLE);
            imageMort.setVisibility(ImageView.INVISIBLE);
        }
        else
        {
            imageContent.setVisibility(ImageView.INVISIBLE);
            imagePasContent.setVisibility(ImageView.INVISIBLE);
            imagePasBien.setVisibility(ImageView.INVISIBLE);
            imageAh.setVisibility(ImageView.INVISIBLE);
            imageMort.setVisibility(ImageView.VISIBLE);
        }

    }
}
