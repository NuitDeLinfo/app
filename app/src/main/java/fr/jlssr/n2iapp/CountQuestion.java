package fr.jlssr.n2iapp;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class CountQuestion {

    private String mot;
    private char lettre;
    private int[] solutions;
    private int correction;
    private Random randGen;
    private String[] dico =
            {
                    "Ornithorynque",
                    "Abracadabrant",
                    "Éclectique",
                    "Anticonstitutionnellement",
                    "Tonitruant",
                    "Zinédine Zidane"
            };

    public int getCorrection() {
        return this.correction;
    }

    public CountQuestion() {
        this.solutions = new int[4];
        this.randGen = new Random();
    }

    public String genWord() {
        int index = this.randGen.nextInt(this.dico.length - 1);
        this.mot = this.dico[index];
        return this.mot;
    }

    public char genChar() {
        this.lettre = this.mot.charAt(this.randGen.nextInt(this.mot.length() - 1));
        return this.lettre;
    }

    public int[] genAns() {
        this.solutions[0] = this.countOccs();
        this.correction = this.solutions[0];
        int potential_bad_solution;
        for (int i = 1; i < 4; i++) {
            do {
                potential_bad_solution = this.randGen.nextInt(this.mot.length() / 2);
            } while (this.contains(potential_bad_solution));
            this.solutions[i] = potential_bad_solution;
        }
        Collections.shuffle(Arrays.asList(this.solutions));
        return this.solutions;
    }

    private int countOccs() {
        int count = 0;
        for (char c : this.mot.toCharArray()) {
            if (this.lettre == Character.toLowerCase(c)) count++;
        }
        return count;
    }

    private boolean contains(int x) {
        boolean is_in = false;
        for (int el : this.solutions) {
            if (el == x) is_in = true;
        }
        return is_in;
    }
}
