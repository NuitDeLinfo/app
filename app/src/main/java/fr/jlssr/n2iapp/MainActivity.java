package fr.jlssr.n2iapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void action1 (View view){
        startActivity(new Intent(this, avant_1.class));
        finish();
    }
    public void action2 (View view){
        startActivity(new Intent(this, CountLetters.class));
        finish();
    }
}

