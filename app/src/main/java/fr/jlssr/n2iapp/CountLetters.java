package fr.jlssr.n2iapp;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CountLetters extends AppCompatActivity {

    //Points max : 10

    // Question courante
    public static CountQuestion quest = new CountQuestion();
    public static CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_letters);
        // Choisir le mot
        String mot = quest.genWord();
        // Choisir la lettre
        char lettre = quest.genChar();
        // Generer les reponses
        int[] reps = quest.genAns();

        // Preparer l'affichage

        // Recuperer les champs de texte
        TextView view_mot = findViewById(R.id.mot);
        TextView view_lettre = findViewById(R.id.lettre);
        final TextView view_chrono = findViewById(R.id.chrono);
        Button btn1 = findViewById(R.id.button1);
        Button btn2 = findViewById(R.id.button2);
        Button btn3 = findViewById(R.id.button3);
        Button btn4 = findViewById(R.id.button4);

        // Associer les valeurs aux champs
        view_mot.setText(mot);
        view_lettre.setText(String.valueOf(lettre));
        view_chrono.setText("10");
        btn1.setText(String.valueOf(reps[0]));
        btn2.setText(String.valueOf(reps[1]));
        btn3.setText(String.valueOf(reps[2]));
        btn4.setText(String.valueOf(reps[3]));
        timer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long remaining_millis) {
                view_chrono.setText("" + remaining_millis / 1000);
            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), "Perdu, + 10 points", Toast.LENGTH_SHORT).show();
                Intent nextQuestion = new Intent(getApplicationContext(), CountDrinks.class);
                startActivity(nextQuestion);
            }
        };
        timer.start();
    }

    // Recuperer la valeur du bouton pressé et la verifier
    public void treatAnswer(View view) {
        Button b = (Button) view;
        String val = b.getText().toString();
        if (goodAnswer(Integer.parseInt(val), quest.getCorrection())) {
            timer.cancel();
            Toast.makeText(getApplicationContext(), "Gagné", Toast.LENGTH_SHORT).show();
            Intent nextQuestion = new Intent(getApplicationContext(), CountDrinks.class);
            startActivity(nextQuestion);
            finish();
        } else {
            timer.cancel();
            ScoreSingleton.getInstance().addPoints(10);
            Toast.makeText(getApplicationContext(), "Perdu, + 10 points", Toast.LENGTH_SHORT).show();
            // Pas le temps de pas me repeter
            Intent nextQuestion = new Intent(getApplicationContext(), CountDrinks.class);
            startActivity(nextQuestion);
            finish();
        }
    }

    // Verifie la réponse
    public boolean goodAnswer(int reponse, int correct) {
        return reponse == correct;
    }

}
