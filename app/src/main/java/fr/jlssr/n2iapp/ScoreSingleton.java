package fr.jlssr.n2iapp;

public class ScoreSingleton {

    private int score;
    private static ScoreSingleton instance;

    private ScoreSingleton(){
        this.score = 0;
    }

    public static synchronized ScoreSingleton getInstance() {
        if (instance == null) {
            instance = new ScoreSingleton();
        }
        return instance;
    }

    public void addPoints(int nb) {
         this.score += nb;
    }

    public int getPoints() {
        return this.score;
    }

}
